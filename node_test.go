package raft

import (
	"context"
	"fmt"
	"log"
	"os"
	"strconv"
	"sync"
	"testing"
	"time"

	"go.uber.org/goleak"
)

const tempDir = "sample-%d"
const proposerFrequency = 50

// ---------------------------------------------------------------------------
// Sample delegate implementation
// ---------------------------------------------------------------------------
type sampleDelegate struct {
	mu    sync.Mutex
	peers map[uint64]*Node
}

func newSampleDelegate() *sampleDelegate {
	return &sampleDelegate{
		peers: make(map[uint64]*Node),
	}
}

func (sd *sampleDelegate) PeerAdded(_ uint64, _ []byte) error {
	return nil
}

func (sd *sampleDelegate) PeerRemoved(_ uint64, _ []byte) error {
	return nil
}

func (sd *sampleDelegate) GetSnapshot() ([]byte, error) {
	return []byte("sample snapshot data"), nil
}

func (sd *sampleDelegate) IsProposalValid(data []byte) bool {
	// Valid proposals are expected to be even integer numbers
	i, err := strconv.Atoi(string(data))
	return err == nil && i%2 == 0
}

func (sd *sampleDelegate) DeliverMessages(msgs []Message) error {
	sd.mu.Lock()
	defer sd.mu.Unlock()
	for _, m := range msgs {
		if _, ok := sd.peers[m.To]; ok {
			_ = sd.peers[m.To].ProcessMessage(context.TODO(), m)
		} else {
			return fmt.Errorf("unknown peer: %d", m.To)
		}
	}
	return nil
}

// ---------------------------------------------------------------------------
// Sample cluster
// ---------------------------------------------------------------------------
type cluster struct {
	mu       sync.Mutex
	peers    []uint64
	nodes    map[uint64]*Node
	delegate *sampleDelegate
}

func newCluster(size int) *cluster {
	cl := &cluster{
		nodes:    make(map[uint64]*Node),
		delegate: newSampleDelegate(),
	}

	// Populate peer ID list
	for i := 1; i <= size; i++ {
		cl.peers = append(cl.peers, uint64(i))
	}

	// Create nodes
	for _, i := range cl.peers {
		n, _ := New(i, &Config{
			WorkDir:  fmt.Sprintf(tempDir, i),
			Join:     false,
			Peers:    cl.peers,
			Delegate: cl.delegate,
		})
		cl.nodes[n.ID] = n
		cl.delegate.peers[i] = n
	}
	return cl
}

func (cl *cluster) start() {
	cl.mu.Lock()
	defer cl.mu.Unlock()
	for _, n := range cl.nodes {
		go handleNodeEvents(n, cl)
		go func(n *Node) {
			_ = n.Start()
		}(n)
	}
}

func (cl *cluster) stop() {
	cl.mu.Lock()
	for _, n := range cl.nodes {
		log.Println("gracefully stopping node:", n.ID)
		if err := n.Stop(); err != nil {
			log.Printf("error when stopping node %d: %s", n.ID, err)
		}
	}
	cl.mu.Unlock()
}

func (cl *cluster) clean() {
	cl.mu.Lock()
	for _, p := range cl.peers {
		_ = os.RemoveAll(fmt.Sprintf(tempDir, p))
	}
	cl.mu.Unlock()
}

func (cl *cluster) addPeer(id uint64) {
	// Create new peer without connection with the existing cluster
	newPeer, _ := New(id, &Config{
		WorkDir:  fmt.Sprintf(tempDir, id),
		Join:     true,
		Peers:    nil,
		Delegate: cl.delegate,
	})

	// Register peer with sample delegate
	// >> Required because is used as a base transport mechanism on the sample code
	cl.delegate.mu.Lock()
	cl.delegate.peers[newPeer.ID] = newPeer
	cl.delegate.mu.Unlock()

	// Register node in sample cluster
	// >> Required to include in clean-up code
	cl.mu.Lock()
	cl.peers = append(cl.peers, newPeer.ID)
	cl.nodes[newPeer.ID] = newPeer
	cl.mu.Unlock()

	go func() {
		_ = newPeer.Start() // Start new peer processing
	}()
	<-newPeer.Ready()                // Wait for the node to be ready-to-roll
	go handleNodeEvents(newPeer, cl) // Handle subsequent events

	// Add new peer to existing cluster
	done := false
	for !done {
		n := cl.randomNode()
		// Notification should be send on any _previously_ existing node
		if n.ID != newPeer.ID {
			wait, err := n.Join(newPeer.ID, nil)
			if err != nil {
				log.Println(">>> failed to add peer:", err)
			}
			<-wait
			done = true
		}
	}
}

func (cl *cluster) removePeer(id uint64) {
	// Request node's removal from the cluster
	cl.mu.Lock()
	defer cl.mu.Unlock()
	if _, err := cl.nodes[id].Resign(); err != nil {
		log.Println(">>> failed to remove peer:", err)
	}
}

func (cl *cluster) randomNode() (n *Node) {
	// 'range' doesn't guarantee the iteration order, so just return the first one
	for _, n = range cl.nodes {
		break
	}
	return n
}

func (cl *cluster) propose(i int) {
	// Dispatch proposals randomly between the existing peers
	cl.mu.Lock()
	cl.randomNode().Propose([]byte(strconv.Itoa(i)))
	cl.mu.Unlock()
}

// ---------------------------------------------------------------------------
// Helper methods
// ---------------------------------------------------------------------------
func handleNodeEvents(n *Node, cl *cluster) {
	i := 0
LOOP:
	for {
		select {
		case <-n.Ready():
			log.Println("node ready", n.ID)

		case c, ok := <-n.Commits():
			if !ok {
				break LOOP
			}
			if c == nil {
				log.Println("=== nil commit received")
				continue
			}
			i++

		case err, ok := <-n.Errors():
			if !ok {
				break LOOP
			}
			if err != nil {
				log.Printf("node %d, report error: %s", n.ID, err)
			}

		case err := <-n.Done():
			log.Printf("done signal for node: %d", n.ID)
			if err != nil {
				log.Printf("fatal error: %s", err)
			}
			// Manually remove it from the cluster
			cl.mu.Lock()
			if _, ok := cl.nodes[n.ID]; ok {
				log.Printf("removing from cluster, node: %d", n.ID)
				delete(cl.nodes, n.ID)
			}
			cl.mu.Unlock()
			break LOOP
		}
	}
	log.Printf("closing node %d, with %d commits received", n.ID, i)
}

func TestCluster(t *testing.T) {
	// Verify for leaked go routines
	defer goleak.VerifyNone(t)

	t.Run("SingleNode", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
		defer cancel()
		cl := newCluster(1)
		cl.start()
		defer cl.clean()

		// Proposer
		i := 0
	LOOP:
		for {
			select {
			case <-ctx.Done():
				break LOOP

			case <-time.Tick(proposerFrequency * time.Millisecond):
				cl.propose(i)
				i++
			}
		}
		log.Println("proposals submitted:", i)
		cl.stop()
	})

	t.Run("MultipleNodes", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.TODO(), 10*time.Second)
		defer cancel()
		cl := newCluster(4)
		cl.start()
		defer cl.clean()

		// Proposer
		i := 0
	LOOP:
		for {
			select {
			case <-ctx.Done():
				break LOOP

			case <-time.Tick(proposerFrequency * time.Millisecond):
				cl.propose(i)
				i++
			}
		}
		log.Println("proposals submitted:", i)
		cl.stop()
	})

	t.Run("AddMember", func(t *testing.T) {
		// Start a 3 peers cluster
		ctx, cancel := context.WithTimeout(context.TODO(), 10*time.Second)
		defer cancel()
		cl := newCluster(3)
		cl.start()
		defer cl.clean()

		// Proposer
		i := 0
	LOOP:
		for {
			select {
			case <-ctx.Done():
				break LOOP

			case <-time.Tick(proposerFrequency * time.Millisecond):
				cl.propose(i)
				i++

				// Add a new peer after 100 successful commits
				if i == 100 {
					log.Println("=== adding new peer ===")
					cl.addPeer(4)
				}
			}
		}
		log.Println("proposals submitted:", i)
		cl.stop()
	})

	t.Run("RemoveMember", func(t *testing.T) {
		// Start a 3 peers cluster
		ctx, cancel := context.WithTimeout(context.TODO(), 10*time.Second)
		defer cancel()
		cl := newCluster(3)
		cl.start()
		defer cl.clean()

		// Proposer
		i := 0
	LOOP:
		for {
			select {
			case <-ctx.Done():
				break LOOP

			case <-time.Tick(proposerFrequency * time.Millisecond):
				cl.propose(i)
				i++

				// Remove a random peer after 100 successful commits
				if i == 100 {
					log.Println("=== remove existing peer ===")
					cl.removePeer(cl.randomNode().ID)
				}
			}
		}
		log.Println("proposals submitted:", i)
		cl.stop()
	})

	t.Run("Leadership", func(t *testing.T) {
		// Start a 3 peers cluster
		ctx, cancel := context.WithTimeout(context.TODO(), 10*time.Second)
		defer cancel()
		cl := newCluster(3)
		cl.start()
		defer cl.clean()

		// Proposer
		i := 0
	LOOP:
		for {
			select {
			case <-ctx.Done():
				break LOOP

			case <-time.Tick(proposerFrequency * time.Millisecond):
				cl.propose(i)
				i++

				// Remove active leader after 100 successful commits
				if i == 100 {
					lead := cl.randomNode().CurrentLeader()
					log.Printf("=== current leader is: %d ===", lead)
					log.Println("=== remove leader to trigger a new election ===")
					if !cl.nodes[lead].IsLeader() {
						t.Error("incorrect leader value received")
					}
					cl.removePeer(lead)
				}

				// Add new peer at 300-commits mark
				if i == 300 {
					log.Println("=== adding new peer ===")
					cl.addPeer(4)
				}
			}
		}
		log.Println("proposals submitted:", i)

		l := 0
		for _, n := range cl.nodes {
			if n.IsLeader() {
				l++
			}
		}
		if l != 1 {
			t.Error("only one leader expected")
		}

		cl.stop()
	})

	t.Run("Politics", func(t *testing.T) {
		// Start a 5 peers cluster and give time for initial leader election
		cl := newCluster(5)
		cl.start()
		defer cl.clean()
		<-time.After(2 * time.Second)

		// Get handlers
		alice := cl.nodes[1]
		bob := cl.nodes[2]
		charlie := cl.nodes[3]
		dave := cl.nodes[4]
		ethan := cl.nodes[5]

		// Make alice the leader, if not already
		if !alice.IsLeader() {
			if err := alice.Campaign(); err != nil {
				t.Fatal("node 1 failed to campaign", err)
			}
			<-time.After(1 * time.Second) // Wait for election
			if !alice.IsLeader() {
				t.Fatal("election failed")
			}
		}

		// Current leader can't campaign
		if err := alice.Campaign(); err == nil {
			t.Fatal("current leader should not be able to campaign")
		}

		// Only the leader can expel members
		if _, err := bob.Expel(charlie.ID, nil); err == nil {
			t.Fatal("only leader should be able to expel members")
		}

		// Leader can't expel itself
		if _, err := alice.Expel(alice.ID, nil); err == nil {
			t.Fatal("leader should not be able to expel itself")
		}

		// Valid expel, leader expel member
		n, err := alice.Expel(dave.ID, nil)
		if err != nil {
			t.Fatal("failed to expel member", err)
		}
		if n == nil {
			t.Fatalf("didn't receive an expel notification handler")
		}

		// Wait for expel notification (i.e., configuration change to be applied)
		// and remove expelled peer from the cluster
		// <-n
		cl.removePeer(dave.ID)

		// Promote another leader
		if err := bob.PromotePeer(ethan.ID); err == nil {
			t.Fatal("only leader can promote other members")
		}
		if err := alice.PromotePeer(ethan.ID); err != nil {
			t.Fatal("failed to promote member")
		}

		// Wait for election
		time.Sleep(1 * time.Second)
		if !ethan.IsLeader() {
			t.Fatal("election failed")
		}

		// Old leader resign
		if _, err := alice.Resign(); err != nil {
			t.Fatal("old leader failed to resign", err)
		}
		cl.stop()
	})
}
