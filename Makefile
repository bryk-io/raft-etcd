.PHONY: all
.DEFAULT_GOAL:=help

## help: Prints this help message
help:
	@echo "Commands available"
	@sed -n 's/^##//p' ${MAKEFILE_LIST} | column -t -s ':' | sed -e 's/^/ /' | sort

## deps: Verify dependencies and remove intermediary products
deps:
	@-rm -rf vendor
	go clean
	go mod tidy
	go mod verify

## docs: Display package documentation on local server
docs:
	@echo "Docs available at: http://localhost:8080/pkg/go.bryk.io/raft-etcd"
	godoc -http=:8080

## test: Run all tests excluding the vendor dependencies
test:
	# Static analysis
	golangci-lint run -v ./...
	go-consistent -v ./...

	# Unit tests
	go test -race -cover -v ./...

## updates: List available updates for direct dependencies
# https://github.com/golang/go/wiki/Modules#how-to-upgrade-and-downgrade-dependencies
updates:
	@go list -u -f '{{if (and (not (or .Main .Indirect)) .Update)}}{{.Path}}: {{.Version}} -> {{.Update.Version}}{{end}}' -m all 2> /dev/null

## scan: Look for knonwn vulnerabilities in the project dependencies
# https://github.com/sonatype-nexus-community/nancy
scan:
	@nancy -quiet go.sum
