package raft

import (
	"encoding/binary"
	"os"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// Turn a valid RFC 4122 unique identifier into a big endian 64 bits uint
func getID(val string) (uint64, error) {
	id, err := uuid.Parse(val)
	if err != nil {
		return 0, errors.New("invalid UUID value")
	}
	if id.Variant() != uuid.RFC4122 {
		return 0, errors.New("invalid UUID variant")
	}
	return binary.BigEndian.Uint64(id[:]), nil
}

// Determine if a path exists and is a directory.
func exists(name string) bool {
	info, err := os.Stat(name)
	return err == nil && info.IsDir()
}
