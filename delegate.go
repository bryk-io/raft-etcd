package raft

import (
	"github.com/coreos/etcd/raft/raftpb"
)

// Message defines internal events to handle during a consensus session
type Message = raftpb.Message

// Delegate provides the necessary functionality to integrate external
// components into the consensus mechanism
type Delegate interface {
	// Perform a validation on the received proposals; if false is returned the
	// entry will not be committed
	IsProposalValid(data []byte) bool

	// Generate a snapshot of current state
	GetSnapshot() ([]byte, error)

	// Handle raft session messages delivery
	DeliverMessages([]Message) error

	// Notification received when a peer is added to the cluster
	PeerAdded(id uint64, data []byte) error

	// Notification received when a peer is removed from the cluster
	PeerRemoved(id uint64, data []byte) error
}
