module go.bryk.io/raft-etcd

go 1.13

require (
	github.com/coreos/etcd v3.3.18+incompatible
	github.com/golang/protobuf v1.3.3 // indirect
	github.com/google/uuid v1.1.1
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.4.0 // indirect
	go.bryk.io/x v0.0.0-20200202181122-5829ad50131f
	go.uber.org/goleak v1.0.0
)
