package raft

import (
	"testing"

	"github.com/google/uuid"
)

func TestUtils(t *testing.T) {
	t.Run("getID", func(t *testing.T) {
		if _, err := getID("not-a-uuid-value"); err == nil || err.Error() != "invalid UUID value" {
			t.Error("failed to catch invalid UUID")
		}
		v4 := uuid.New()
		if n, err := getID(v4.String()); n == 0 || err != nil {
			t.Error("failed to process valid value", err)
		}
	})
}
