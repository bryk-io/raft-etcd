/*
Package raft provides an implementation for the "Raft" consensus algorithm.

Raft is a protocol with which a cluster of nodes can maintain a replicated state
machine. The state machine is kept in sync through the use of a replicated log.

This package provides a wrapper using the original implementation by CoreOS used
for "etcd". The original code can be found here: https://github.com/etcd-io/etcd

More information is available on the original paper by Diego Ongaro and John Ousterhout.
https://raft.github.io/raft.pdf
*/
package raft
