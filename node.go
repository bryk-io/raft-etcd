package raft

import (
	"context"
	"fmt"
	"log"
	"os"
	"path"
	"sync"
	"time"

	r "github.com/coreos/etcd/raft"
	"github.com/coreos/etcd/raft/raftpb"
	"github.com/coreos/etcd/snap"
	"github.com/coreos/etcd/wal"
	"github.com/coreos/etcd/wal/walpb"
	"github.com/pkg/errors"
	xl "go.bryk.io/x/log"
)

const (
	snapshotCatchUpEntries uint64 = 1000
	defaultSnapshotCount   uint64 = 1000
	defaultHeartbeat       uint   = 100
	maxInFlightMessage     int    = 256
	maxMessageSize         uint64 = 1024 * 1024
)

// Config provides the basic node configuration parameters
type Config struct {
	// Should be a writable and durable path, required files for execution will be
	// stored at this location.
	WorkDir string

	// Indicate if the node is joining an existing cluster.
	Join bool

	// List of existing nodes in the cluster.
	Peers []uint64

	// Integration delegate.
	Delegate Delegate

	// Handle node's output.
	Log xl.Logger

	// Time between internal state advancements, in milliseconds.
	Heartbeat uint
}

// Node defines the main integration point to participate in a raft consensus session.
// nolint: maligned
type Node struct {
	// ID, should be unique through all the life of the cluster (non reusable)
	ID   uint64
	conf *Config

	// communications
	proposeC       chan []byte // receive entries to be proposed
	commitC        chan []byte // deliver entries committed
	errorC         chan error  // deliver processing errors
	doneC          chan error  // signal clients when node is about to finish execution
	readyC         chan bool   // signal when the node is readyC-to-use by clients
	processingDone chan bool   // signal when the main event loop is complete

	// state
	bailC           chan bool
	ready           bool
	lastIndex       uint64
	appliedIndex    uint64
	snapshotIndex   uint64
	snapCount       uint64
	confChangeCount uint64
	confState       raftpb.ConfState
	mu              sync.Mutex

	// internals
	walDir      string                   // path to WAL directory
	snapDir     string                   // path to snapshot directory
	rn          r.Node                   // base library node
	wal         *wal.WAL                 // log handler
	ticker      *time.Ticker             // node ticker to advance internal timers
	storage     *r.MemoryStorage         // local storage
	snapshotter *snap.Snapshotter        // snapshots handler
	ctx         context.Context          // base node context, used to manage processing routines
	halt        context.CancelFunc       // stop node's internal context
	waitList    map[uint64]chan struct{} // operations waiting for notifications
}

// New prepares a new raft node instance for usage; the provided 'id' must be unique
// through all the lifecycle of the cluster (non reusable).
func New(id uint64, conf *Config) (*Node, error) {
	// Working directory
	if !exists(conf.WorkDir) {
		if err := os.Mkdir(conf.WorkDir, 0750); err != nil {
			return nil, errors.Wrap(err, "failed to create working directory")
		}
	}

	// Snapshot directory
	snapDir := path.Join(conf.WorkDir, "snap")
	if !exists(snapDir) {
		if err := os.Mkdir(snapDir, 0750); err != nil {
			return nil, errors.Wrap(err, "failed to create snapshot directory")
		}
	}

	// Set a default heartbeat value of 100 ms
	if conf.Heartbeat == 0 {
		conf.Heartbeat = defaultHeartbeat
	}

	ctx, cancel := context.WithCancel(context.TODO())
	return &Node{
		ID:              id,
		ctx:             ctx,
		halt:            cancel,
		conf:            conf,
		ticker:          time.NewTicker(time.Duration(conf.Heartbeat) * time.Millisecond),
		proposeC:        make(chan []byte),
		readyC:          make(chan bool),
		commitC:         make(chan []byte),
		errorC:          make(chan error),
		doneC:           make(chan error, 1),
		bailC:           make(chan bool, 1),
		processingDone:  make(chan bool, 1),
		walDir:          path.Join(conf.WorkDir, "wal"),
		snapshotter:     snap.New(snapDir),
		snapDir:         snapDir,
		snapCount:       defaultSnapshotCount,
		storage:         r.NewMemoryStorage(),
		ready:           false,
		confChangeCount: 0,
		waitList:        make(map[uint64]chan struct{}),
	}, nil
}

// Commits provides a read-only channel where committed entries can be received.
func (n *Node) Commits() <-chan []byte {
	n.mu.Lock()
	c := n.commitC
	n.mu.Unlock()
	return c
}

// Errors provides a read-only channel where execution errors can be received.
func (n *Node) Errors() <-chan error {
	n.mu.Lock()
	c := n.errorC
	n.mu.Unlock()
	return c
}

// Done will signal clients that the node has complete it's execution,
// no more proposals should be send. If an error is returned it must be considered fatal.
func (n *Node) Done() <-chan error {
	n.mu.Lock()
	c := n.doneC
	n.mu.Unlock()
	return c
}

// Ready signal when the node it's readyC to use by clients.
func (n *Node) Ready() <-chan bool {
	n.mu.Lock()
	c := n.readyC
	n.mu.Unlock()
	return c
}

// Propose a new entry for a consensus round.
func (n *Node) Propose(entry []byte) {
	n.proposeC <- entry
}

// Start initiates the internal node processing.
func (n *Node) Start() error {
	// Consider the node as restarting if a previous WAL already exists
	restarting := wal.Exist(n.walDir)

	// Load WAL
	var err error
	if n.wal, err = n.loadWAL(); err != nil {
		return err
	}

	// Internal handler configuration
	c := &r.Config{
		ID:              n.ID,
		ElectionTick:    10,
		HeartbeatTick:   1,
		Storage:         n.storage,
		MaxSizePerMsg:   maxMessageSize,
		MaxInflightMsgs: maxInFlightMessage,
	}
	if n.conf.Log != nil {
		c.Logger = n.conf.Log
	}

	// Properly start or restart internal handler
	n.mu.Lock()
	if !restarting {
		// Generate peer list, including self
		rPeers := []r.Peer{{ID: n.ID}}
		for _, p := range n.conf.Peers {
			rPeers = append(rPeers, r.Peer{ID: p})
		}

		// Start the node with an empty list of peers to allow joins in the future.
		if n.conf.Join {
			rPeers = nil
		}
		n.rn = r.StartNode(c, rPeers)
	} else {
		n.rn = r.RestartNode(c)
	}
	n.mu.Unlock()

	// Restore state from last snapshot
	ss, err := n.storage.Snapshot()
	if err != nil {
		return err
	}
	n.mu.Lock()
	n.confState = ss.Metadata.ConfState
	n.snapshotIndex = ss.Metadata.Index
	n.appliedIndex = ss.Metadata.Index
	n.mu.Unlock()

	// Start events processing
	go n.eventsProcessing()
	return nil
}

// ProcessMessage will handle incoming consensus session messages.
func (n *Node) ProcessMessage(ctx context.Context, m Message) error {
	n.mu.Lock()
	defer n.mu.Unlock()
	return n.rn.Step(ctx, m)
}

// Join will submit a configuration change to add a new peer to the cluster.
func (n *Node) Join(nid uint64, data []byte) (chan struct{}, error) {
	n.mu.Lock()
	defer n.mu.Unlock()
	n.confChangeCount++
	cc := raftpb.ConfChange{
		Type:    raftpb.ConfChangeAddNode,
		ID:      n.confChangeCount,
		NodeID:  nid,
		Context: data,
	}
	if err := n.rn.ProposeConfChange(context.TODO(), cc); err != nil {
		return nil, err
	}
	n.waitList[n.confChangeCount] = make(chan struct{})
	return n.waitList[n.confChangeCount], nil
}

// Resign will make the node to leave the cluster.
func (n *Node) Resign() (chan struct{}, error) {
	n.mu.Lock()
	defer n.mu.Unlock()
	n.confChangeCount++
	cc := raftpb.ConfChange{
		Type:    raftpb.ConfChangeRemoveNode,
		ID:      n.confChangeCount,
		NodeID:  n.ID,
		Context: nil,
	}
	if err := n.rn.ProposeConfChange(context.TODO(), cc); err != nil {
		return nil, err
	}
	n.waitList[n.confChangeCount] = make(chan struct{})
	return n.waitList[n.confChangeCount], nil
}

// Expel will remove an existing peer from the cluster. Only the current leader
// can expel members and can't expel itself
func (n *Node) Expel(nid uint64, data []byte) (chan struct{}, error) {
	if !n.IsLeader() {
		return nil, errors.New("only current leader can expel members")
	}
	if n.ID == nid {
		return nil, errors.New("can't expel itself")
	}
	n.mu.Lock()
	defer n.mu.Unlock()
	n.confChangeCount++
	cc := raftpb.ConfChange{
		Type:    raftpb.ConfChangeRemoveNode,
		ID:      n.confChangeCount,
		NodeID:  nid,
		Context: data,
	}
	if err := n.rn.ProposeConfChange(context.TODO(), cc); err != nil {
		return nil, err
	}
	n.waitList[n.confChangeCount] = make(chan struct{})
	return n.waitList[n.confChangeCount], nil
}

// Campaign causes the node to transition to candidate state and start campaigning to become leader.
// The current leader can't start a campaign.
func (n *Node) Campaign() error {
	if n.IsLeader() {
		return errors.New("already current leader")
	}
	return n.rn.Campaign(context.TODO())
}

// PromotePeer will attempt to transfer leadership to the given peer. Only the current leader
// can promote another peer for leadership.
func (n *Node) PromotePeer(nid uint64) error {
	if !n.IsLeader() {
		return errors.New("only current leader can promote peers")
	}
	n.rn.TransferLeadership(context.TODO(), n.ID, nid)
	return nil
}

// IsLeader return true if the node is currently the elected leader for the raft session.
func (n *Node) IsLeader() bool {
	return n.CurrentLeader() == n.ID
}

// CurrentLeader return ID of the peer currently serving as session leader.
func (n *Node) CurrentLeader() uint64 {
	n.mu.Lock()
	defer n.mu.Unlock()
	return n.rn.Status().Lead
}

// Stop will properly terminate all internal node processing.
func (n *Node) Stop() error {
	n.stop(nil)
	return nil
}

// Internal clean-up.
func (n *Node) stop(err error) {
	n.halt()           // Stop processing loops
	<-n.processingDone // Wait for the main processing loop to complete
	n.rn.Stop()        // Finish underlying raft session
	n.doneC <- err     // Notify clients, the channel is buffered and won't block
	close(n.proposeC)  // Close proposals channel
	close(n.commitC)   // Close commits channel
	close(n.errorC)    // Close errors channel
	_ = n.wal.Close()
}

// Save a snapshot to the WAL and snapshot handler; release the acquired lock afterwards.
func (n *Node) saveSnapshot(snap raftpb.Snapshot) error {
	walSnap := walpb.Snapshot{
		Index: snap.Metadata.Index,
		Term:  snap.Metadata.Term,
	}
	if err := n.wal.SaveSnapshot(walSnap); err != nil {
		return errors.Wrap(err, "failed to save snapshot to WAL")
	}
	if err := n.snapshotter.SaveSnap(snap); err != nil {
		return errors.Wrap(err, "failed to save snapshot to handler")
	}
	return errors.Wrap(n.wal.ReleaseLockTo(snap.Metadata.Index), "failed to release lock")
}

// Produce a new snapshot if required.
func (n *Node) triggerSnapshot() error {
	if n.appliedIndex-n.snapshotIndex <= n.snapCount {
		return nil
	}

	// Create and save snapshot
	data, err := n.conf.Delegate.GetSnapshot()
	if err != nil {
		return errors.Wrap(err, "failed to retrieve snapshot data")
	}
	ss, err := n.storage.CreateSnapshot(n.appliedIndex, &n.confState, data)
	if err != nil {
		return errors.Wrap(err, "failed to create snapshot")
	}
	if err := n.saveSnapshot(ss); err != nil {
		return errors.Wrap(err, "failed to save snapshot")
	}

	// Run compaction
	compactIndex := uint64(1)
	if n.appliedIndex > snapshotCatchUpEntries {
		compactIndex = n.appliedIndex - snapshotCatchUpEntries
	}
	if err := n.storage.Compact(compactIndex); err != nil {
		return errors.Wrap(err, "failed to compact storage")
	}

	// Update state
	n.mu.Lock()
	n.snapshotIndex = n.appliedIndex
	n.mu.Unlock()
	return nil
}

// Save and apply a received snapshot.
func (n *Node) handleIncomingSnapshot(ss raftpb.Snapshot) error {
	if r.IsEmptySnap(ss) {
		return nil
	}
	if err := n.saveSnapshot(ss); err != nil {
		return errors.Wrap(err, "failed to save snapshot")
	}
	if err := n.storage.ApplySnapshot(ss); err != nil {
		return errors.Wrap(err, "failed to apply snapshot")
	}
	if ss.Metadata.Index <= n.appliedIndex {
		return fmt.Errorf("snapshot index [%d] should > progress.appliedIndex [%d] + 1", ss.Metadata.Index, n.appliedIndex)
	}
	n.mu.Lock()
	n.confState = ss.Metadata.ConfState
	n.snapshotIndex = ss.Metadata.Index
	n.appliedIndex = ss.Metadata.Index
	n.mu.Unlock()
	return nil
}

// Open and existing WAL or start a new one if required.
func (n *Node) openWAL(snapshot *raftpb.Snapshot) (*wal.WAL, error) {
	// Create new WAL if it doesn't exist
	if !wal.Exist(n.walDir) {
		if err := os.Mkdir(n.walDir, 0750); err != nil {
			return nil, errors.Wrap(err, "failed to create WAL directory")
		}
		w, err := wal.Create(n.walDir, nil)
		if err != nil {
			return nil, errors.Wrap(err, "failed to create WAL")
		}
		if err := w.Close(); err != nil {
			return nil, errors.Wrap(err, "failed to close newly created WAL")
		}
	}

	// Set snapshot index and term if provided
	sp := walpb.Snapshot{}
	if snapshot != nil {
		sp.Index, sp.Term = snapshot.Metadata.Index, snapshot.Metadata.Term
	}

	// Open existing WAL
	w, err := wal.Open(n.walDir, sp)
	if err != nil {
		return nil, errors.Wrap(err, "failed to open existing WAL")
	}
	return w, nil
}

// Prepare node's WAL (write-ahead-log).
func (n *Node) loadWAL() (*wal.WAL, error) {
	// Load snapshot if available
	snapshot, err := n.snapshotter.Load()
	if err != nil && err != snap.ErrNoSnapshot {
		return nil, errors.Wrap(err, "failed to load snapshot")
	}

	// Prepare node's WAL
	w, err := n.openWAL(snapshot)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// Read WAL contents
	_, st, entries, err := w.ReadAll()
	if err != nil {
		return nil, errors.Wrap(err, "failed to read WAL contents")
	}

	// Apply snapshot it provided
	if snapshot != nil {
		if err := n.storage.ApplySnapshot(*snapshot); err != nil {
			return nil, errors.Wrap(err, "failed to apply snapshot")
		}
	}

	// Set state
	if err := n.storage.SetHardState(st); err != nil {
		return nil, errors.Wrap(err, "failed to set WAL state")
	}

	// Append entries
	if err := n.storage.Append(entries); err != nil {
		return nil, errors.Wrap(err, " failed to append WAL entries")
	}

	if len(entries) > 0 {
		// Adjust index
		n.mu.Lock()
		n.lastIndex = entries[len(entries)-1].Index
		n.mu.Unlock()
	} else {
		// Reload of previous entries is complete
		if !n.ready {
			n.ready = true
			n.readyC <- true
		}
	}
	return w, nil
}

// Verify entries indexes.
func (n *Node) filterEntries(source []raftpb.Entry) ([]raftpb.Entry, error) {
	var list []raftpb.Entry
	if len(source) == 0 {
		return list, nil
	}
	first := source[0].Index
	if first > n.appliedIndex+1 {
		return nil, errors.Errorf("start index [%d] should <= appliedIndex[%d]+1", first, n.appliedIndex)
	}
	if n.appliedIndex-first+1 < uint64(len(source)) {
		list = source[n.appliedIndex-first+1:]
	}
	return list, nil
}

// Handle received configuration adjustments.
func (n *Node) applyConfChange(cc raftpb.ConfChange) {
	n.confState = *n.rn.ApplyConfChange(cc)

	// Deliver operation notification
	n.mu.Lock()
	if notify, ok := n.waitList[cc.ID]; ok {
		close(notify)
		delete(n.waitList, cc.ID)
	}
	n.mu.Unlock()

	// Handle request
	switch cc.Type {
	case raftpb.ConfChangeAddNode:
		// Notify delegate of the new peer
		if err := n.conf.Delegate.PeerAdded(cc.NodeID, cc.Context); err != nil {
			n.errorC <- err
		}
	case raftpb.ConfChangeRemoveNode:
		// Node was expelled
		if cc.NodeID == n.ID {
			n.bailC <- true
			break
		}

		// Notify delegate of removed peer
		if err := n.conf.Delegate.PeerRemoved(cc.NodeID, cc.Context); err != nil {
			n.errorC <- err
		}
	}
}

// Process entries received as part of the raft session execution.
func (n *Node) handleEntries(entries []raftpb.Entry) bool {
	for _, entry := range entries {
		switch entry.Type {
		case raftpb.EntryNormal:
			// Don't re-process/re-commit replied entries
			if !n.ready {
				break
			}

			// Ignore empty messages
			if len(entry.Data) == 0 {
				break
			}

			// Validate proposal
			if !n.conf.Delegate.IsProposalValid(entry.Data) {
				break
			}

			select {
			case n.commitC <- entry.Data:
			case <-n.ctx.Done():
				return false
			}

		case raftpb.EntryConfChange:
			var cc raftpb.ConfChange
			if err := cc.Unmarshal(entry.Data); err != nil {
				n.errorC <- err
				break
			}
			n.applyConfChange(cc)
		}

		// after commit, update appliedIndex
		n.appliedIndex = entry.Index
		if entry.Index == n.lastIndex {
			// Replay finished
			if !n.ready {
				log.Println(">>> reply finished")
				n.ready = true
				n.readyC <- true
			}
		}
	}
	return true
}

// Submit incoming proposed entries, will terminate when stopping node's processing.
func (n *Node) dispatchProposals() {
	for {
		select {
		case prop, ok := <-n.proposeC:
			if !ok {
				return
			}
			// blocks until accepted by raft state machine
			if err := n.rn.Propose(context.TODO(), prop); err != nil {
				n.errorC <- err
			}

		case <-n.ctx.Done():
			return
		}
	}
}

// Main processing loop.
func (n *Node) eventsProcessing() {
	defer n.ticker.Stop()

	// Submit incoming proposals
	go n.dispatchProposals()

	// event loop on raft state machine updates
	for {
		select {
		case <-n.bailC:
			n.processingDone <- true
			n.stop(nil)
			return

		case <-n.ctx.Done():
			n.processingDone <- true
			return

		case <-n.ticker.C:
			n.rn.Tick()

		case rd := <-n.rn.Ready():
			if err := n.wal.Save(rd.HardState, rd.Entries); err != nil {
				n.stop(err) // Fatal error
			}

			// Handle snapshot
			if !r.IsEmptySnap(rd.Snapshot) {
				if err := n.handleIncomingSnapshot(rd.Snapshot); err != nil {
					n.stop(err) // Fatal error
				}
			}

			// Update local storage
			if err := n.storage.Append(rd.Entries); err != nil {
				n.stop(err) // Fatal error
			}

			// Use delegate to deliver session messages
			if err := n.conf.Delegate.DeliverMessages(rd.Messages); err != nil {
				// Client will decide how to manage the error
				n.errorC <- err
			}

			// Handle entries
			list, err := n.filterEntries(rd.CommittedEntries)
			if err != nil {
				n.stop(err) // Fatal error
			}
			if ok := n.handleEntries(list); !ok {
				// Client will decide how to manage the error
				n.errorC <- errors.New("missing entries")
			}
			if err := n.triggerSnapshot(); err != nil {
				n.stop(err) // Fatal error
			}
			n.rn.Advance()
		}
	}
}
